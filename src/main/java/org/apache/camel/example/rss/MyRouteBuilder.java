/**
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.apache.camel.example.rss;

import javax.xml.transform.dom.DOMSource;

import org.apache.camel.Exchange;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.file.FileComponent;
import org.apache.camel.example.rss.StringLFAggregationStrategy;
import org.apache.camel.model.language.XQueryExpression;
import org.w3c.dom.Document;


/**
 * A simple example router demonstrating the camel-rss component.
 */
public class MyRouteBuilder extends RouteBuilder {



    public void configure() {

    	from("file:target/classes/inbox?charset=UTF-8")
    	   .to("log:rss0?showBodyType=true&showBody=false&showProperties=true")
    	 // nyni je to GenericFile  
    	 .to("xslt:xslt/remove-descr.xsl")
    	   .to("log:removed-descr?showBodyType=true&showBody=false&showProperties=true")
    	  // nyni je to String
    	   .setHeader("CamelFileName",simple("${exchangeId}.xml"))
    	   .setHeader("RssId",simple("${exchangeId}"))
    	   .to("file:target/rss-dumps-no-descr?charset=UTF-8&tempFileName=${file:onlyname.noext}.tmp")    	
    	   .multicast()
    	     .to("direct:split-items")
    	     .to("direct:extract-links")    	     		
    	     .to("direct:extract-enclosures")    	     		
    	     .to("direct:extract-titles")    	     		
    	   .end();

    	   from("direct:split-items")
    	   .to("log:before-split?showBodyType=true&showBody=false&showProperties=true")
    	   .split(xpath("/rss/channel/item"))
     	       .convertBodyTo(String.class,"UTF-8") // naprosto nezbytne, jinak zmrsi kodovani!
     	       .setHeader("CamelFileName",simple("${header.RssId}-${property.CamelSplitIndex}-item.xml"))
     	       .to("file:target/split-by-item?charset=UTF-8&tempFileName=${file:onlyname.noext}.tmp")
    	    .end() // konec splitovani
    	   // melo by byt (bez definice strategy) identicke  puvodnimi soubory...
    	   .to("file:target/after-split-items")
    	;

    	from("direct:extract-links")
    	   .split(xpath("/rss/channel/item"),
    			  new StringLFAggregationStrategy())
    	          	  .setBody(xpath("/item/link/text()"))
    	    .end() // konec splitovani
    	 .setHeader("CamelFileName",simple("${header.RssId}-links.txt"))
    	 .to("log:links-aggregate?showBody=true&showProperties=true")
    	 .to("file:target/split-by-item?charset=UTF-8&tempFileName=${file:onlyname.noext}.tmp")    	  
    	;
    	
    	from("direct:extract-titles")
 	   .split(xpath("/rss/channel/item"),
 			  new StringLFAggregationStrategy())
 	          	  .setBody(xpath("/item/title/text()"))
 	          	  //.transform().xquery("concat(/item/pubDate,/item/title/text())",String.class)

   	   .end() // konec splitovani
 	 .setHeader("CamelFileName",simple("${header.RssId}-titles.txt"))
 	 .to("log:titles-aggregate?showBody=true&showProperties=true")
 	 .to("file:target/split-by-item?charset=UTF-8&tempFileName=${file:onlyname.noext}.tmp")    	  
 	;

    	
   	 from("direct:extract-enclosures")
 	   .split(xpath("/rss/channel/item"),
 			  new StringLFAggregationStrategy())
 	          	  .setBody(xpath("/item/enclosure/@url"))
 	          	  .convertBodyTo(String.class)
 	          	  .to("direct:fetch-url")
 	    .end() // konec splitovani
 	 .setHeader("CamelFileName",simple("${header.RssId}-enclosures.txt"))
 	 .to("log:enclosures-agg?showBody=true&showProperties=true")
 	 .to("file:target/split-by-item?charset=UTF-8&tempFileName=${file:onlyname.noext}.tmp")    	  
 	;
    
   	from("direct:fetch-url")
   	  .to("log:fetch-url?showBodyType=true&showBody=true")   	  
   	  .filter(simple("${body} regex '^http://.*'"))
   	    .setHeader("myURL",body())
   	    .to("log:ffff?showHeaders=true")
   	    .setHeader(Exchange.HTTP_URI, header("myURL"))   	    
   	    .setHeader(Exchange.HTTP_METHOD,simple("GET"))   	    
   	    .to("http://dummy")
    	.setHeader("CamelFileName",
    			    header("myURL").regexReplaceAll("(.*/)", "").regexReplaceAll("\\?.*", ""))
   	    .to("log:fetched?showBodyType=true&showBody=false&showHeaders=true")
   	    .to("file:target/split-by-item/enclosures")
   	  .end()
   	;
   	 
    }
}
