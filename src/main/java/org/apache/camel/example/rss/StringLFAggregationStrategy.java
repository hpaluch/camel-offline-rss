package org.apache.camel.example.rss;

import org.apache.camel.Exchange;
import org.apache.camel.processor.aggregate.AggregationStrategy;

public class StringLFAggregationStrategy implements AggregationStrategy {

	@Override
	public Exchange aggregate(Exchange oldExchange, Exchange newExchange) {
		 if (oldExchange == null) {
			 return newExchange;
		 }
    String oldBody = oldExchange.getIn().getBody(String.class);
    String newBody = newExchange.getIn().getBody(String.class);
    if (oldBody!=null && oldBody.trim().length()>0){
    	oldBody += "\r\n";
    }
    oldExchange.getIn().setBody(oldBody + newBody);
    return oldExchange;
	}

}
